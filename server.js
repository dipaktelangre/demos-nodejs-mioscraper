var express = require('express');
var fs = require('fs');
var http = require('http');
var request = require('request');
var cheerio = require('cheerio');
var mkdirp = require('mkdirp');
var url = require('url');

var exec = require('child_process').exec;
var spawn = require('child_process').spawn;
var app = express();

app.get('/scrape', function (req, res) {

    //All the web scraping magic will happen here
    console.log("I get initialized ...");
    console.log(req.query);
    //url = 'http://mio.to/album/Tarihi';
    //url='http://mio.to/album/Amit+Trivedi%2C+Arijit+Singh/Fitoor+%282016%29';
    url = req.query.album_url

    request(url, function (error, response, html) {
        var DOWNLOAD_DIR = '/downloads';

        if (!error) {
            // Next, we'll utilize the cheerio library on the returned html which will essentially give us jQuery functionality
            var $ = cheerio.load(html);
            var mp3_server = "http://media-audio.mio.to/";
            var cover = $(".group").first().find("img").attr("src");
            cover_details = cover.split("/").slice(3);
            cover_details.pop();
            var artist = cover_details.join("/") + "/";
            track_url_extension = "-vbr-V5.mp3";
            var track_list = [];
            $(".track-list tr").each(function (index, track) {
                $track = $(this);
                var track_detail = {};
                track_detail.album_id = $track.attr("album_id");
                track_detail.track_number = $track.attr("track_number");
                track_detail.disc_number = $track.attr("disc_number");
                track_detail.genre = $track.attr("genre");
                track_detail.track_name = $track.attr("track_name");
                track_detail.track_url = mp3_server + artist + encodeURIComponent(track_detail.disc_number + "_" + track_detail.track_number + " - " + track_detail.track_name + track_url_extension);
                //track_detail.track_url = encodeURIComponent(track_detail.track_url);
                track_list.push(track_detail);
                console.log(track_detail);

            });

            console.log("=================================================");
            console.log("Track List Scrapping completed ");


            console.log("Downloading started");
            var dest_dir = __dirname + DOWNLOAD_DIR + track_list[0].genre + "/";
            var download_counter = 0;
            var initDownload = function () {
                var track = track_list[download_counter];
                var file_path = dest_dir + track.track_name + ".mp3";
				console.log("Downloading ... " + track.track_name);
                download(track.track_url, file_path, function (err) {
                    console.log("File Downloaded==> " + track.track_name + " Current Counter " + download_counter + "/" + track_list.length);
                    download_counter++;
                    if (err) {
                        console.log("Error ==> " + err);
                    }
                    if (download_counter < track_list.length) {

                        initDownload();
                    }
                    if (download_counter == track_list.length) {     
						console.log("Download Completed ");
                    }

                });
            };

            if (!fs.existsSync(dest_dir)) {
                //fs.mkdirSync(dest_dir);
                console.log("creating dir ==> " + dest_dir);
                mkdirp(dest_dir, function (err) {
                    if (err) console.error(err)
                    else {
                        console.log('dir created!');
                        initDownload();
                    }
                });

            }
            else{
            	initDownload();
            } 

            
            res.json(track_list);

            
        }
    });

});




var download = function (url, dest, cb) {
    var file = fs.createWriteStream(dest);	
    var request = http.get(url, function (response) {
        response.pipe(file);
        file.on('finish', function () {
            file.close(cb); // close() is async, call cb after close completes.
            console.log("file saved ==>" + dest);
        });
    }).on('error', function (err) { // Handle errors
        //fs.unlink(dest); // Delete the file async. (But we don't check the result)
        if (cb) cb(err.message);
    });
};

app.listen('8081')

console.log('Magic happens on port 8081');

exports = module.exports = app;