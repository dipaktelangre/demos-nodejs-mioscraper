# README #

This is test MOI(Musics of India) scraper 

### What is this repository for? ###

* MIO Scraper
* 1.1


### How do I get set up? ###

* Git clone 
* npm install
* npm start server.js

### Usage  ###

* localhost:8081/scrape?album_url=http://mio.to/album/Raj+Kapoor+Collection

* album_url :- copy album url from moi site
* Check download folder 
